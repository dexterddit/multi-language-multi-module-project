Multi-language multi-module sample project on Gitlab
=======================
![Quality Gate](http://sonarqube.com/api/badges/gate?key=gitlab-multi-language-multi-module-project)

:exclamation:This is  a fork of :
https://github.com/bellingard/multi-language-multi-module-project

#### This project is analysed on [SonarQube.com](https://sonarqube.com)!
Scan output:
Analysis is performed:
- On the master branch and  the results are pushed to SonarQube.com
- On merge(pull) requestes when a merge request is made ( should add the option for merge request to be have gree build in order merged)
Scan results:

https://sonarqube.com/dashboard/index/gitlab-multi-language-multi-module-project

#### Featured languages
- **Java** - with coverage
- **JavaScript** - with coverage
- **Flex** - with coverage
- **PHP** - with coverage
- **Groovy** - with coverage

#### How to run an analysis on your SonarQube instance?


##### :construction_worker:How does the gitlab-ci work :

We use a maven docker container (maven:3.3.9-jdk-8-alpine) which we will use to download the sonar-runner in order to scan our example project :
> gitlab-ci.yml

>image: maven:3.3.9-jdk-8-alpine
>before_script:
>- mvn -version
>- java -version
>build:
>  stage: build
>  script:
>  - echo $HOME
>  - chmod +x runSonarQubeAnalysis.sh
>  - ./runSonarQubeAnalysis.sh

The runSonarQubeAnalysis.sh will take care of installing the runner and running the scan .Don't forget to add the 2 Environment variables($SONAR_TOKEN and $SONAR_URL) in gitlab variables
